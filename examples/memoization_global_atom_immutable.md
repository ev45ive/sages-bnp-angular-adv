```js

makeWidget = (fn) => {
    let cache = ''
    let prevData = ''
    
    return (data) => {
        if(data === prevData && cache) return cache;
        prevData = data;
        return cache = fn(data)
    }
}
widgetA = makeWidget(d => ({renderedA:d}));
widgetB = makeWidget(d => ({renderedB:d}));

makeApp = (data) => ({
    widgetA: widgetA(data.A),
    widgetB: widgetB(data.B)
})
data1 = {A:123,B:234};
a = makeApp(data1)
// {widgetA: {…}, widgetB: {…}}

data2 = {...data1,B:3245};
b = makeApp(data2)
// {widgetA: {…}, widgetB: {…}}

a.widgetA === b.widgetA
true // No cahnge -> No rerender

a.widgetB === b.widgetB
false // Change -> Rerendered

```
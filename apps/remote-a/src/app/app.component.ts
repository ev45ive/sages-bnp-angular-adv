import { Component } from '@angular/core';

@Component({
  selector: 'remote-a-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'remote-a';
}

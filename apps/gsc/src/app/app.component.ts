import { Component } from '@angular/core';
import { AuthService } from './core/services/auth/auth.service';

@Component({
  selector: 'bnp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'Sages BNP Angular';

  constructor(
    private auth: AuthService
  ) { }
  
  login() {
    this.auth.login()
  }
}

import { Inject, ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NxWelcomeComponent } from '../nx-welcome.component';
import { SharedModule } from '../shared/shared.module';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { INITIAL_PLAYLISTS_DATA } from '../playlists/tokens';
import { HttpClient, HttpClientModule, HttpHandler, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthConfig, OAuthModule } from 'angular-oauth2-oidc';
import { environment } from '../../environments/environment';
import { AuthService } from './services/auth/auth.service';
import { AuthInterceptor } from './services/auth.interceptor';
import { NgxsModule } from '@ngxs/store';
import { PlaylistsState } from './store/playlists.state';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';


@NgModule({
  declarations: [
    NxWelcomeComponent,
    PageNotFoundComponent,
  ],
  imports: [
    CommonModule,
    OAuthModule.forRoot(),
    SharedModule,
    HttpClientModule,
    NgxsModule.forRoot([
      PlaylistsState
    ], {
      developmentMode: !environment.production
    }),
    !environment.production ? NgxsReduxDevtoolsPluginModule.forRoot() : []
  ],
  exports: [
    NxWelcomeComponent,
    PageNotFoundComponent
  ],
  providers: [
    {
      provide: AuthConfig,
      useValue: environment.authConfig
    },
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: AuthInterceptor
    },
    // Override default providers:
    // {
    //   provide: HttpHandler,
    //   useClass: MyMuchBetterAwesomeHttpClient
    // },
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: 'standard' }
    }
  ]
})
export class CoreModule {


  constructor(
    private auth: AuthService,
    // private http: HttpClient,
    @Inject(INITIAL_PLAYLISTS_DATA)
    private playlist = []) {
    // console.log(playlist);
    auth.init()
  }

  static forRoot(config: {}): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule, providers: [
      ]
    }
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'apps/gsc/src/environments/environment';
import { catchError, map, of, throwError } from 'rxjs';
import { Album, AlbumSearchResponse } from '../../model/Search';
import { AuthService } from '../auth/auth.service';
import { mockAlbums } from './mockAlbums';

@Injectable({
  providedIn: 'root'
})
export class AlbumSearchService {

  results: Album[] = mockAlbums

  constructor(
    private http: HttpClient,
  ) { }

  searchAlbums(query = '') {
    return this.http.get<AlbumSearchResponse>(
      'search', {
      params: { type: 'album', query },

    }).pipe(
      map(data => data.albums.items),
    )
  }
}

import { ErrorHandler, Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { catchError, Observable, throwError } from 'rxjs';
import { AuthService } from './auth/auth.service';
import { environment } from 'apps/gsc/src/environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  private api_url = environment.api_url
  constructor(
    private errorHandler: ErrorHandler,
    private auth: AuthService,

  ) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    request = request.clone({
      url: this.api_url + request.url,
      setHeaders: {
        Authorization: `Bearer ` + this.auth.getToken()
      }
    })

    return next.handle(request).pipe(
      catchError((error: unknown) => {
        this.errorHandler.handleError(error)

        if (error instanceof HttpErrorResponse) {
          return throwError(() => new Error(
            error.error.error.message
          ))
        }

        return throwError(() => new Error('Unexpected Error'))
      })
    );
  }
}

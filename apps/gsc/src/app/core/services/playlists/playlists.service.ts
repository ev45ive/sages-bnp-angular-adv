import { HttpClient } from '@angular/common/http';
import { EventEmitter, Inject, Injectable, Optional } from '@angular/core';
import { BehaviorSubject, map, of, ReplaySubject, Subject, tap } from 'rxjs';
import { INITIAL_PLAYLISTS_DATA } from '../../../playlists/tokens';
import { Playlist } from '../../model/Playlist';
import { PagingObject } from '../../model/Search';

@Injectable({
  providedIn: 'root'
})
export class PlaylistsService {


  constructor(
    @Optional() @Inject(INITIAL_PLAYLISTS_DATA)
    private data: Playlist[],
    private http: HttpClient
  ) {
    this.data = this.data || [];

    (window as any).subject = this.playlistsChange
  }

  // Multicast Observable - 1 to *
  // playlistsChange = new EventEmitter<Playlist[]>(true /* isAsync */)
  // playlistsChange = new Subject<Playlist[]>()
  playlistsChange = new ReplaySubject<Playlist[]>(1, 10_000)
  // playlistsChange = new AsyncSubject<Playlist[]>() //.complete()
  // playlistsChange = new BehaviorSubject<Playlist[]>(this.data)

  getPlaylistById(id: string) {
    // Unicast Observable - 1 to 1
    return of(this.data.find(p => p.id === id)).pipe(
      tap(() => console.log('request playlist ' + id))
    )
  }

  savePlaylist(draft: Playlist) {
    this.data = this.data.map(p => {
      return p.id === draft.id ? draft : p
    })
    setTimeout(() => {
      this.playlistsChange.next(this.data)
    }, 1000);
  }

  getPlaylists() {
    setTimeout(() => {
      this.playlistsChange.next(this.data)
    }, 1000);
  }

  fetchPlaylists() {
    return this.http.get<PagingObject<Playlist>>('me/playlists').pipe(map(res => res.items))
  }

  fetchPlaylistById(id: string) {
    return this.http.get<Playlist>('playlists/' + id)
  }
}


// [Store]
//   |
// [Service]
//   | 
// [Component]
import { Playlist } from "./Playlist";

export const playlistsMockData: Playlist[] = [{
  id: '123',
  name: 'Playlist 123',
  public: false,
  description: 'Best playlist',
}, {
  id: '234',
  name: 'Playlist 234',
  public: true,
  description: 'Top playlist',
}, {
  id: '345',
  name: 'Playlist 345',
  public: false,
  description: 'Last playlist',
}];

import { Playlist } from "../model/Playlist"
import { State, StateContext, Action, Selector } from '@ngxs/store';
import { Injectable } from "@angular/core";
import { PlaylistsService } from '../services/playlists/playlists.service'
import { LoadPlaylist, LoadPlaylists, SavePlaylist, SelectPlaylist } from "./playlists.actions";

type PlaylistsStateModel = {
  items: Playlist[],
  selectedId?: Playlist['id']
}

@State<PlaylistsStateModel>({
  name: 'Playlists',
  defaults: {
    items: []
  }
})
@Injectable()
export class PlaylistsState {
  constructor(private service: PlaylistsService) { }

  @Action(LoadPlaylists)
  loadPlaylists(ctx: StateContext<PlaylistsStateModel>) {
    const state = ctx.getState();

    this.service.fetchPlaylists().subscribe(items => {
      
      
      ctx.setState({
        ...state,
        items
      });
    })
  }

  @Action(LoadPlaylist)
  loadPlaylist(ctx: StateContext<PlaylistsStateModel>, action: LoadPlaylist) {

    const state = ctx.getState();

    this.service.fetchPlaylistById(action.id).subscribe(data => {
      ctx.setState({
        ...state,
        items: state.items.map(p => p.id === data.id ? data : p)
      });
    })
  }

  @Action(SelectPlaylist)
  selectPlaylist(ctx: StateContext<PlaylistsStateModel>, action: SelectPlaylist) {

    const state = ctx.getState();

    this.service.fetchPlaylistById(action.id).subscribe(data => {
      ctx.setState({
        ...state,
        selectedId: action.id
      });
    })
  }

  @Action(SavePlaylist)
  savePlaylist(ctx: StateContext<PlaylistsStateModel>, action: SavePlaylist) {

    const state = ctx.getState();
    const data = action.draft

    ctx.setState({
      ...state, items: state.items.map(p => p.id === data.id ? data : p)

    });
  }

  @Selector()
  static playlists(state: PlaylistsStateModel) {
    return state.items
  }

  @Selector()
  static selected(state: PlaylistsStateModel) {
    return state.items.find(p => p.id === state.selectedId)
  }


}
import { Playlist } from "../model/Playlist";


export class LoadPlaylists {
  static readonly type = '[Playlits] Load All';
  constructor() { }
}

export class PlaylistsLoaded {
  static readonly type = '[Playlits] Loaded All';
  constructor(public data: Playlist[]) { }
}

export class LoadPlaylist {
  static readonly type = '[Playlits] Load One';
  constructor(public id: string) { }
}

export class SelectPlaylist {
  static readonly type = '[Playlits] Select';
  constructor(public id: string) { }
}

export class SavePlaylist {
  static readonly type = '[Playlits] Save';
  constructor(public draft: Playlist) { }
}
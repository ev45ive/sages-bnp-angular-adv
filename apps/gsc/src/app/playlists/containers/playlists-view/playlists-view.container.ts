import { NgIf } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, shareReplay, Subscription, switchMap } from 'rxjs';
import { Playlist } from "../../../core/model/Playlist";
import { State, StateContext, Action, Store, Select } from '@ngxs/store';
import { PlaylistsState } from '../../../core/store/playlists.state';
import { LoadPlaylist, LoadPlaylists, SavePlaylist, SelectPlaylist } from '../../../core/store/playlists.actions';

type DisplayModes = 'details' | 'editor';

@Component({
  selector: 'bnp-playlists-view',
  templateUrl: './playlists-view.container.html',
  styleUrls: ['./playlists-view.container.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistsViewContainer implements OnInit {

  selectedId = new BehaviorSubject<string>('')
  mode = new BehaviorSubject<DisplayModes>('details')

  // playlists = this.store.select(state => state.Playlists.items);
  @Select(PlaylistsState.playlists) playlists!: Observable<Playlist[]>;
  @Select(PlaylistsState.selected) selected!: Observable<Playlist | undefined>;

  constructor(private store: Store) { }

  ngOnInit(): void {
    this.store.dispatch(new LoadPlaylists())
  }

  select(id: Playlist['id']) {
    this.store.dispatch(new SelectPlaylist(id))
  }

  savePlaylist(draft: Playlist) {
    this.store.dispatch(new SavePlaylist(draft))
      .subscribe(() => {
        this.mode.next('details')
      })
  }

  editMode() {
    this.mode.next('editor')
  }

  cancel() {
    this.mode.next('details')
  }



}

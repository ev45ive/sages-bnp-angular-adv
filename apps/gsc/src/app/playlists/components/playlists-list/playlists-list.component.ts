import { NgForOf, NgForOfContext, NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../../core/model/Playlist';

NgIf
NgForOf
NgForOfContext

@Component({
  selector: 'bnp-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush // internal event / OnChanges
})
export class PlaylistsListComponent implements OnInit {

  @Input('items') playlists: Playlist[] = []

  @Input() selectedId?: string | null = ''

  @Output() selectedIdChange = new EventEmitter<Playlist['id']>();

  select(id: Playlist['id']) {
    this.selectedIdChange.emit(id)
  }

  constructor() { }

  ngOnInit(): void {
  }

  random() {
    console.log('Rerender list');

  }

}

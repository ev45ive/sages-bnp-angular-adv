import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bnp-playlists-list-item',
  templateUrl: './playlists-list-item.component.html',
  styleUrls: ['./playlists-list-item.component.css']
})
export class PlaylistsListItemComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

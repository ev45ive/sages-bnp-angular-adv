import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlaylistsViewContainer } from './containers/playlists-view/playlists-view.container';
import { PlaylistsComponent } from './playlists.component';

const routes: Routes = [
  {
    path: '', component: 
    PlaylistsComponent, pathMatch: 'prefix',
    children: [
      {
        path: '', 
        component: PlaylistsViewContainer
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule] // re-export
})
export class PlaylistsRoutingModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistsComponent } from './playlists.component';
import { PlaylistsViewContainer } from './containers/playlists-view/playlists-view.container';
import { PlaylistsListComponent } from './components/playlists-list/playlists-list.component';
import { PlaylistsListItemComponent } from './components/playlists-list-item/playlists-list-item.component';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { PlaylistEditorComponent } from './components/playlist-editor/playlist-editor.component';
import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';
import { playlistsMockData } from '../core/model/playlistsMockData';
import { INITIAL_PLAYLISTS_DATA } from './tokens';
import { PlaylistsService } from '../core/services/playlists/playlists.service';
import { Playlist } from '../core/model/Playlist';

@NgModule({
  declarations: [
    PlaylistsComponent,
    PlaylistsViewContainer,
    PlaylistsListComponent,
    PlaylistsListItemComponent,
    PlaylistDetailsComponent,
    PlaylistEditorComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PlaylistsRoutingModule,
    // RouterModule.forChild(routes)
  ],
  providers: [
    // {
    //   provide: INITIAL_PLAYLISTS_DATA,
    //   useValue: playlistsMockData,
    // },
    // PlaylistsService
    // {
    //   provide: PlaylistsService,
    //   useClass: PlaylistsService
    //   // deps: [INITIAL_PLAYLISTS_DATA]
    // },
    // {
    //   provide: PlaylistsService,
    //   useFactory(data: Playlist[] = []) {
    //     return new PlaylistsService(data)
    //   },
    //   deps: [INITIAL_PLAYLISTS_DATA]
    // }
    // Override:
    // {
    //   provide: INITIAL_PLAYLISTS_DATA,
    //   useValue: playlistsMockData
    // },
  ]
})
export class PlaylistsModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, catchError, debounceTime, EMPTY, map, Observable, switchMap } from 'rxjs';
import { Album } from '../../../core/model/Search';
import { AlbumSearchService } from '../../../core/services/album-search/album-search.service';

@Component({
  selector: 'bnp-album-search-view',
  templateUrl: './album-search-view.container.html',
  styleUrls: ['./album-search-view.container.css']
})
export class AlbumSearchViewContainer implements OnInit {
  message = ''

  query = this.route.queryParamMap.pipe(
    map(pm => pm.get('q'))
  )

  results = this.query.pipe(
    debounceTime(500),
    switchMap(query => {
      if (!query) return EMPTY;

      return this.service
        .searchAlbums(query || '')
        .pipe(
          catchError(err => {
            this.message = err.message
            return EMPTY
          })
        )
    }),
  )

  constructor(
    private service: AlbumSearchService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
  }

  submitSearch(query: string) {

    this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: { q: query }
    })

  }
}

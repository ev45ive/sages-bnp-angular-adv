import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bnp-album-detail-view',
  templateUrl: './album-detail-view.container.html',
  styleUrls: ['./album-detail-view.container.css']
})
export class AlbumDetailViewContainer implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

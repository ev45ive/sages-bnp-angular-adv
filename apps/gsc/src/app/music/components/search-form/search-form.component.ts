import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'bnp-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {

  @Input() set query(query: string | null) {
    this.searchForm.setValue({
      query: query || ''
    })
  }
  @Output() search = new EventEmitter<string>();

  searchForm = new FormGroup({
    'query': new FormControl('batman')
  })

  constructor() {
    (window as any).form = this.searchForm
  }

  submit() {
    this.search.next(this.searchForm.value['query'])
  }

  ngOnInit(): void {
  }

}

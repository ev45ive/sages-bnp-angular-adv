import { Component, Input, OnInit } from '@angular/core';
import { Album } from '../../../core/model/Search';

@Component({
  selector: 'bnp-results-grid',
  templateUrl: './results-grid.component.html',
  styleUrls: ['./results-grid.component.css']
})
export class ResultsGridComponent implements OnInit {

  @Input() results!: Album[]

  constructor() { }

  ngOnInit(): void {
  }

}

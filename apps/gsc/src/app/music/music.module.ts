import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { MusicRoutingModule } from './music-routing.module';
import { MusicComponent } from './music.component';
import { AlbumSearchViewContainer } from './containers/album-search-view/album-search-view.container';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { ResultsGridComponent } from './components/results-grid/results-grid.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { AlbumDetailViewContainer } from './containers/album-detail-view/album-detail-view.container';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    MusicComponent,
    AlbumSearchViewContainer,
    SearchFormComponent,
    ResultsGridComponent,
    AlbumCardComponent,
    AlbumDetailViewContainer
  ],
  imports: [
    SharedModule,
    CommonModule,
    MusicRoutingModule,
  ]
})
export class MusicModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlbumDetailViewContainer } from './containers/album-detail-view/album-detail-view.container';
import { AlbumSearchViewContainer } from './containers/album-search-view/album-search-view.container';
import { MusicComponent } from './music.component';

const routes: Routes = [{
  path: '', component: MusicComponent,
  children: [
    { path: '', pathMatch: 'full', redirectTo: 'albums/search' },
    { path: 'albums/search', component: AlbumSearchViewContainer },
    { path: 'albums', component: AlbumDetailViewContainer }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicRoutingModule { }

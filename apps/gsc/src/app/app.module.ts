import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';

import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './core/components/page-not-found/page-not-found.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { playlistsMockData } from './core/model/playlistsMockData';
import { INITIAL_PLAYLISTS_DATA } from './playlists/tokens';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';

const routes: Routes = [
  {
    path: '', redirectTo: '/music/albums/search', pathMatch: 'full',
  },
  {
    path: 'playlists', loadChildren: () =>
      import('./playlists/playlists.module').then(m => m.PlaylistsModule)
  },
  {
    path: 'music', loadChildren: () =>
      import('./music/music.module').then(m => m.MusicModule)
  },
  // { path: '', component: HomeComponent },
  // { path: 'path', component: FeatureComponent },

  // nx g c core/components/PageNotFound --export
  { path: '**', component: PageNotFoundComponent },
];


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true
    }),
    BrowserModule,
    // PlaylistsModule
    CoreModule.forRoot({}),
    SharedModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: INITIAL_PLAYLISTS_DATA,
      useValue: playlistsMockData,
    },
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: 'standard' }
    }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }

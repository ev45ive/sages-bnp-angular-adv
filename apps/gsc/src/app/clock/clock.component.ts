import { ChangeDetectionStrategy, ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';

@Component({
  selector: 'bnp-clock',
  template: ` 
    {{random()}}
    <p>{{time}}</p>
  `,
  styles: [
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClockComponent implements OnInit {

  time = (new Date()).toLocaleTimeString()

  constructor(
    private zone: NgZone,
    private cdr: ChangeDetectorRef) {
    // this.cdr.detach()
  }

  ngOnInit(): void {
    // this.cdr.detectChanges()

    this.zone.runOutsideAngular(() => {
      setInterval(() => {
        this.time = (new Date()).toLocaleTimeString()
        this.cdr.detectChanges()
      }, 1e3)
    })
  }

  random() {
    // console.log('REnder clock');

  }
}

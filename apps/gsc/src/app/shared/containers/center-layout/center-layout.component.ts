import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'bnp-center-layout',
  templateUrl: './center-layout.component.html',
  styleUrls: ['./center-layout.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CenterLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

## GIT

git clone https://bitbucket.org/ev45ive/sages-bnp-angular-adv.git sages-bnp-angular-adv
cd sages-bnp-angular-adv
npm install
npm run nx s

http://localhost:4200/

## Problemy z GIT / NPM

- sprawdz proxy

git config --global http.sslVerify false
git config --global http.postBuffer 1048576000

## Instalacje

node -v
v16.13.1

npm -v
8.1.2

git --version
git version 2.31.1.windows.1

Visual Studio Code
Help -> About -> Version 1.66

chrome://version
Google Chrome 99.0.4844.84

## NVM

https://github.com/nvm-sh/nvm#install--update-script
https://github.com/nvm-sh/nvm#long-term-support

## Angular CLI / Nx Workspaces

npm i -g nx create-nx-workspace @angular/cli

ng version
@angular-devkit/architect 0.1303.1 (cli-only)
@angular-devkit/core 13.3.1 (cli-only)
@angular-devkit/schematics 13.3.1 (cli-only)
@schematics/angular 13.3.1 (cli-only)

npx create-nx-workspace nazwa_projektu
npx ng new nazwa_projektu

# Docker node

docker run -it -v $(pwd):/home -p 4200:4200 -w /home node bash

docker run -it -v ${pwd}:/home -p 4200:4200 -w /home node bash

MSYS_NO_PATHCONV=1 docker run -it -v $(pwd):/home -p 4200:4200 -w /home node bash

## API

https://api.spotify.com/v1

## Semver

https://semver.org/
https://semver.npmjs.com/

## DevOps - CI - install exact from package-lock.json

npm ci

## UI toolkit

https://www.ngdevelop.tech/best-angular-ui-component-libraries/

https://material.io/
https://www.primefaces.org/primeng/#/setup
https://ng-bootstrap.github.io/#/getting-started
https://mdbootstrap.com/docs/angular/
https://material.angular.io/
https://valor-software.com/ngx-bootstrap/#/

## VS Code Extensions

https://marketplace.visualstudio.com/items?itemName=Angular.ng-template

https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode
https://marketplace.visualstudio.com/items?itemName=nrwl.angular-console
https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

## Angular IVY + Webpack

https://www.npmjs.com/package/@ngtools/webpack
https://www.youtube.com/watch?v=anphffaCZrQ&ab_channel=AngularConnect

## Generators / Schematics

npx nx g c clock -is -it

npx nx generate @schematics/angular:component --name=clock --project=gsc --module=app --style=scss --inlineStyle --inlineTemplate --no-interactive

npx nx generate @schematics/angular:module --name=core --module=app
npx nx generate @schematics/angular:module --name=shared --module=app

## Playlist module outline
npx nx generate @schematics/angular:module --name=playlists --module=app --route=playlists --routing

npx nx generate @schematics/angular:component playlists/containers/PlaylistsView --type=container

npx nx generate @schematics/angular:component playlists/components/PlaylistsList  
npx nx generate @schematics/angular:component playlists/components/PlaylistsListItem  
npx nx generate @schematics/angular:component playlists/components/PlaylistDetails  
npx nx generate @schematics/angular:component playlists/components/PlaylistEditor  


## Material Design
npm install @angular/material && npx nx g @angular/material:ng-add

√ Choose a prebuilt theme name, or "custom" for a custom theme: · indigo-pink
√ Set up global Angular Material typography styles? (y/N) · true
√ Set up browser animations for Angular Material? (Y/n) · true

npx nx generate @angular/material:navigation --name=shared/containers/navigation --style=scss --export --routing 

## Flex Layout Module
npm i -s @angular/flex-layout @angular/cdk



## Music search module outline
npx nx generate @schematics/angular:interface core/model/Search  
npx nx generate @schematics/angular:service core/services/AlbumSearch --flat false  

npx nx generate @schematics/angular:module --name=music --module=app --route=music --routing

npx nx generate @schematics/angular:component music/containers/AlbumSearchView --type=container

npx nx generate @schematics/angular:component music/components/SearchForm  
npx nx generate @schematics/angular:component music/components/ResultsGrid  
npx nx generate @schematics/angular:component music/components/AlbumCard  


npx nx generate @schematics/angular:component music/containers/AlbumDetailView --type=container


## Auth

https://www.npmjs.com/package/angular-oauth2-oidc

## RxJS
https://rxmarbles.com/#map
https://rxjs.dev/operator-decision-tree
https://www.learnrxjs.io/
https://rxviz.com/examples/mouse-move


## MicorFrontend - Webpack MOdule Federation

https://stackoverflow.com/questions/70135006/upgrade-angular-from-12-to-13-added-can-not-use-import-meta-outside-a-module
https://webpack.js.org/concepts/module-federation/
https://nx.dev/guides/setup-mfe-with-angular

### HOST 
npx nx generate @nrwl/angular:application --name=hostapp --port=4201  --mfe --mfeType=host --routing  --e2eTestRunner=none --prefix=host --skipTests --standaloneConfig --unitTestRunner=none

### Remote A
npx nx generate @nrwl/angular:application --name=remoteA --port=4202  --host=hostapp --mfe --prefix=remote-a --routing --e2eTestRunner=none --skipTests --standaloneConfig --unitTestRunner=none

### Remote B
npx nx generate @nrwl/angular:application --name=remoteB --port=4203  --host=hostapp --mfe --prefix=remote-a --routing --e2eTestRunner=none --skipTests --standaloneConfig --unitTestRunner=none

https://angular.io/guide/elements


## Redux in Angular:
https://ngrx.io/
https://www.ngxs.io/

https://www.ngxs.io/plugins/devtools